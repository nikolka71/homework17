// homework17.cpp : This file contains the 'main' function. Program execution begins and ends there.
//


#include <iostream>
#include <vector>

class Example
{
private:
    int x;
public:
    Example()
    {
        x = 0;
    }

    Example(int newX)
    {
        x = newX;
    }
    int GetX()
    {
        return x;
    }

    void SetX(int newX)
    {
        x = newX;
    }
};

class Vector
{
public:
    Vector() : x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    double b()
    {
        return   sqrt(x * x + y * y + z * z);
    }
    void Show()
    {

        std::cout << b();//
    }
private:
    double x;
    double y;
    double z;

};

int main()
{
    Example temp(15);
    std::cout << temp.GetX() << "\n";

    Vector v(10, 20, 30);
    v.Show();



}


// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
